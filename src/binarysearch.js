/*
==============================================
Binary Search:
In computer science, a binary search or half-interval search algorithm finds the position of a target value within a sorted array.

==============================================
Algorithm:
The binary search algorithm begins by comparing the target value to the value of the middle element of the sorted array.
If the target value is equal to the middle element's value, then the position is returned and the search is finished.
If the target value is less than the middle element's value, then the search continues on the lower half of the array; or 
if the target value is greater than the middle element's value, then the search continues on the upper half of the array.
This process continues, eliminating half of the elements, and comparing the target value to the value of the middle element
of the remaining elements - until the target value is either found (and its associated element position is returned), or 
until the entire array has been searched (and "not found" is returned).

==============================================
Links:
https://en.wikipedia.org/wiki/Binary_search_algorithm

*/
function BinarySearch(data) {

  if (data instanceof Array) {
    this.data = data;
  } else {
    throw "Input data must be an Array";
  }

  this.search = function (target) {
    this.target = target;
    var scope = data;

    console.log('==================');
    console.log('target: "' + target + '"');
    var result = searchWork(scope, target);
    console.log('result: "' + result + '"');
    return result;
  };

  var searchWork = function (scope, target) {
    console.log('scope: "' + scope + '"');

    var x = middleElement(scope);
    console.log('compare target with: "' + x + '"');    
    if (x === target) return true;

    var idx = middleIndex(scope);
    if (target < x) scope = scope.slice(0, idx);
    if (target > x) scope = scope.slice(idx + 1);

    return searchWork(scope, target);
  };

  var middleIndex = function (data) {
      return Math.floor(data.length / 2);
  }

  var middleElement = function (data) {
    var idx = middleIndex(data);
    return data[idx];
  }
}

  