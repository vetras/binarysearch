describe("Binary Search Spec", function() {
  var instance;
  var testData = [1, 3, 4, 6, 8, 9, 11,20,23,24,12,1234,12,234,25,456,677,65,24,56,486,97,2];
  testData.sort(function(a, b){return a-b});

  beforeEach(function() {
    instance = new BinarySearch(testData);
  });
  
  describe("Search", function() {
    it("should be able to find existing element (middle)", function() {
      expect(instance.search(6)).toEqual(true);  
    });
  });
 
  describe("Search", function() {
    it("should be able to find existing element (first)", function() {
      var first = testData[0];
      expect(instance.search(first)).toEqual(true);  
    });
  });
  
    describe("Search", function() {
    it("should be able to find existing element (last)", function() {
      var last = testData[testData.length-1];
      expect(instance.search(last)).toEqual(true);  
    });
  });
  
});